#include <iostream>

using namespace std;

//initialise grid avec des 0 dans toutes les cases du tableau
void initGrid(int grid[][6]) {
	for (int line = 0; line < 6; line++) {
		for (int column = 0; column < 6; column++) {
			grid[line][column] = 0;
		}
	}
}

void showGrid (int grid[][6]) {
	cout << endl;
	cout << "         -   -   -   -   -   -   -" << endl;
	for (int line = 0; line < 6; line++) {
		cout << "         |";
		for (int column = 0; column < 6; column++) {
			// Affichage en fonction de la valeur
			cout << " ";
			switch (grid[line][column]) {
			case 1:
				cout << "O"; // affichage du caract�re O pour le joueur 1
				break;
			case 2:
				cout << "X"; // affichage du caract�re X pour le joueur 2
				break;
			default:
				cout << " "; // affichage d'un espace par d�faut
				break;
			}
			cout << " |";
		}
		cout << endl << "         -------------------------" << endl;
	}
	cout << endl;
}

void turnPlayer(int grid[][6], int player) {
	bool validHit = false; // varible informat sur la validit� du coup
	while (!validHit) {
		int choosedColumn = 0; // initialisation
		//Force le joueur a choisir une colonne valide
		while (choosedColumn < 1 || choosedColumn > 6) {
			cout << "Choose your column between 1 and 6 : ";
			cin >> choosedColumn;
		}
		choosedColumn--; // Pour avoir le bonne indice dans le tableau grid (colonne 1 = indice 0 !)
		
		// On part de la fin du tableau
		for (int line = 5; line >= 0; line--) {
			// on verifie qu(il n'y a pas de piece dans la case la plus en bas et on remonte si c'est le cas
			if (grid[line][choosedColumn] == 0) {
				grid[line][choosedColumn] = player;
				validHit = true;
				break; // pernet de sortir de la boucle for plus vite
			}
		}
		//affichage de l'erreur si la colonne est d�j� pleine de pi�ces
		if (!validHit) {
			cout << "Column invalid (it's full of pieces)." << endl;
		}
	}
}

//verifie si il y a une ligne de 4 en fonction de la position d'une piece
bool checkLineFor(int grid[][6], int line, int column) {
	if (grid[line][column] != 0) {
		return (grid[line][column] == grid[line][column + 1]
			&& grid[line][column] == grid[line][column + 2]
			&& grid[line][column] == grid[line][column + 3]);
	}
	return false;
}

//verifie si il y a une colonne de 4 en fonction de la position d'une piece
bool checkColumnFor(int grid[][6], int line, int column) {
	if (grid[line][column] != 0) {
		return (grid[line][column] == grid[line + 1][column]
			&& grid[line][column] == grid[line + 2][column]
			&& grid[line][column] == grid[line + 3][column]);
	}
	return false;
}

//verifie si il y a une diagonale allant d'en haut � gauche � en bas � droite de 4 en fonction de la position d'une piece
bool checkDiagonalTopLeftToBotRightFor(int grid[][6], int line, int column) {
	if (grid[line][column] != 0) {
		return (grid[line][column] == grid[line + 1][column + 1]
			&& grid[line][column] == grid[line + 2][column + 2]
			&& grid[line][column] == grid[line + 3][column + 3]);
	}
	return false;
}

//verifie si il y a une diagonale allant d'en bas � gauche � en haut � droite de 4 en fonction de la position d'une piece
bool checkDiagonalBotLeftToTopRightFor(int grid[][6], int line, int column) {
	if (grid[line][column] != 0) {
		return (grid[line][column] == grid[line - 1][column + 1]
			&& grid[line][column] == grid[line - 2][column + 2]
			&& grid[line][column] == grid[line - 3][column + 3]);
	}
	return false;
}

bool checkPower4(int grid[][6]) {
	bool power4 = false;
	for (int line = 0; line < 6; line++) {
		for (int column = 0; column < 6; column++) {
			if (column < 3) {
				if (line < 3) {
					power4 = power4 || checkLineFor(grid, line, column) || checkColumnFor(grid, line, column) || checkDiagonalTopLeftToBotRightFor(grid, line, column);
				}
				else {
					power4 = power4 || checkLineFor(grid, line, column) || checkDiagonalBotLeftToTopRightFor(grid, line, column);
				}
			}
			else if (line >= 3){
				power4 = power4 || checkColumnFor(grid, line, column);
			}
		}
	}
	return power4;
}

int main()
{
	int grid[6][6]; // tableau repr�sentant la grille de jeu
	int newGame; // variable pour savoir si le jeu s'arrete
	//Lancement du jeu pour la premi�re fois
	do {
		cout << endl << "-------------- Power 4 Start --------------" << endl << endl;

		initGrid(grid); //
		int turn = 0; // variable qui s'incr�mente pour g�rer le num�ro du joueur actuel
		int winner = -1; // numero du gagnant du jeu
		newGame = -1;
		showGrid(grid); // affiche la grille de jeu actuelle

		//tant que le gagnant n'a pas �t� trouv�
		while (winner == -1) {
			// test le cas ou le tableau est plein
			if (turn == 36) {
				winner = 0; // pour sortir de la boucle et afficher le cas du match nul
			}
			else {
				system("CLS");
				cout << endl << "-------------- Power 4 Start --------------" << endl << endl;
				cout << "------------ Player " << turn % 2 + 1 << "'s turn ! ------------" << endl;
				showGrid(grid); // affiche la grille de jeu actuelle
				// "turn % 2 + 1" corespond au num�ro joueur (1 ou 2) en fonction de turn qui s'incr�mente a chaque tour de jeu
				turnPlayer(grid, turn % 2 + 1);
				system("CLS");
				cout << endl << "-------------- Power 4 Start --------------" << endl << endl;
				cout << "------------ Player " << turn % 2 + 1 << "'s turn ! ------------" << endl;
				cout << endl;
			}
			if (turn >=6 && checkPower4(grid)) {
				winner = turn % 2 + 1; //"turn % 2 + 1" corespond au num�ro joueur (1 ou 2) en fonction de turn qui s'incr�mente a chaque tour de jeu
			}
			//system("PAUSE");
			turn++;
		}
		if (winner == 0) {
			cout << "This is draw : there is no winner." << endl;
		}
		else {
			cout << "The winner is player " << winner <<  "." << endl;
		}

		//Tant que la valeur n'est pas 0 ou 1 (attention : si l'on met un caract�re on sort de la boucle car la valeur de newGame devient 0)
		while (newGame != 0 && newGame != 1) {
			cout << "Do you want to play another game of Power 4 ? (1 for yes, 0 or a character for no) : ";
			cin >> newGame;
		}
	} while (newGame == 1);
	return 0;
}